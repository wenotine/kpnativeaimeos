<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', 'HomeController@index');

/*Route::match( array( 'GET', 'POST' ), '/{locale}/{currency}/{f_name}/{f_catid?}/{f_sort?}', array(
		'as' => 'aimeos_shop_list',
		'uses' => '\Aimeos\Shop\Controller\CatalogController@listAction'
));*/

Route::group(['prefix'=>'{site}/{locale}/{currency}'],function(){
	Route::match( array( 'GET', 'POST' ), '{f_name?}/{f_catid?}/{f_sort?}', array(
			'as' => 'aimeos_shop_list',
			'uses' => '\Aimeos\Shop\Controller\CatalogController@listAction'
	))->where('sublevels', '.*');;
});





